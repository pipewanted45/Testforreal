import Vue from "vue";
import Vuex from "vuex";

import {db} from '../components/firebase';

Vue.use(Vuex);

const user = {
  state: {
    appointment: []
  },

  mutations: {
    addappointment(state, { payload }) {
      state.appointment.push(payload);
    },
    deleteinfo(state, { index }) {
      state.appointment.splice(index, 1);
    },
    editinfo(state, { payload }) {
      
      var {U_Username , U_Fname , U_Lname , U_Email , U_Tel , U_Pic , index} = payload;
      state.appointment[index] =
      {
        U_Username , 
        U_Fname , 
        U_Lname , 
        U_Email , 
        U_Tel ,
        U_Pic
      };
    },
    fetchList(state , res){
      state.appointment = res
      console.log(state.appointment)
    }
  },

  actions: {
    addappointment({ commit }, payload) {
      commit("addappointment", { payload });
    },

    deleteinfo({ commit }, payload) {
      db.ref("User_ID").orderByChild("U_Username").equalTo(payload.username).once('value',(snapshot) => {
        db.ref("User_ID").child(snapshot.key).remove().then(() =>commit("deleteinfo", { payload}));
  
      })
    
    },
    editinfo({ commit }, payload) {
      var {U_Username , 
        U_Fname , 
        U_Lname , 
        U_Email ,
         U_Tel , 
         U_Pic
        } = payload;
      
      console.log(payload)

      db.ref("User_ID").orderByChild("U_Username").equalTo(U_Username).once('value',(sp) => {
        var key = Object.keys(sp.val())[0]
        
        db.ref("User_ID").child(key).update({
          U_Username , U_Fname , U_Lname , U_Email , U_Tel , U_Pic
        }).then(() =>  commit("editinfo", { payload }))
      
      })
     
    },
    fetchList({commit}){
      db.ref('User_ID').on("value",function (snapshot){
        commit("fetchList",snapshot.val());
      })
      
    }

  },  

  getters: {
    appointment: state => state.appointment
  }
}

const foods = {
  state: {
    food: []
  },

  mutations : {
    fetchfood(state , res){
      state.food = res;
      console.log(res)
    },
    editfood(state, { payload }) {
      
      var {name , cal , pic , index} = payload;
      state.food[index] =
      {
        name , cal , pic
      };
    },
    deletefood(state, {payload}){
state.food.splice(payload.index,1);
    }
  },
  actions: {
    fetchfood({commit}){
   
      db.ref('Food').on("value",function (snapshot){
        commit("fetchfood",snapshot.val());
      })
    },
    editfood({ commit }, payload) {
      var {name , cal , pic ,  index } = payload;
      
      console.log(payload)

      db.ref("Food").child(index).update({
        name,cal,pic
      }).then(() =>  commit("editfood", { payload }))
      
     
    },
    deletefood({commit} , payload) {
      db.ref("Food").child(payload.index).remove().then(() => commit("deletefood",{payload}))
    }
  },
  

  getters: {
    food: state => state.food
  }

}

const exercises = {
  state: {
    exercise: []
  },

  mutations : {
    fetchexercise(state , res){
      state.exercise = res;
      console.log(res)
    },
    editexercise(state, { payload }) {
      
      var {name , amount , time , cal , advantage , index} = payload;
      state.exercise[index] =
      {
        name , amount , time , cal , advantage
      };
    },
    deleteexercise(state, {payload}){
state.exercise.splice(payload.index,1);
    }
  },
  actions: {
    fetchexercise({commit}){
   
      db.ref('Exercise').on("value",function (snapshot){
        commit("fetchexercise",snapshot.val());
      })
    },
    editexercise({ commit }, payload) {
      var {name , amount , time , cal , advantage ,  index } = payload;
      
      console.log(payload)

      db.ref("Exercise").child(index).update({
        name,amount,time,cal,advantage
      }).then(() =>  commit("editexercise", { payload }))
      
     
    },
    deleteexercise({commit} , payload) {
      db.ref("Exercise").child(payload.index).remove().then(() => commit("deleteexercise",{payload}))
    }
  },
  

  getters: {
    exercise: state => state.exercise
  }

}



export const store = new Vuex.Store({
 modules:{
   a : user,
   b : foods,
   c : exercises
 }
});